---
title: Fundamentals of version control written tutorial
...

In this tutorial we are going through the basics of version control with Git and Gitlab. We will discuss how to set up a sustainable and correct workflow, how to create a final product that is completely version controlled while being able to go back and forth between different **versions**, correcting **mistakes** and compare **features**.

## The case study
For this tutorial we are going to make a _"mock"_ manuscript that could be used for a publication combining **LaTeX** for the creation of the document and **Python** for the generation of some mock data and a figure.

Below you can find:

* The [presentation](https://mvgroup.gitlab.io/version_control_tutorial/docs/presentation/) associated with this tutorial.
* The [Gitlab repository](https://gitlab.com/mvgroup/version_control_tutorial/-/tree/main/) for our course on version control.

## Description of the folder and files

* Manuscript/**manuscript.tex** -> main file for the production of the final manuscript. Compile it to obtain the final PDF.
* Manuscript/**figures** -> folder where images and figures to include in the manuscript are stored. In this case the figure is generated from Python and saved directly in this directory.
* **python_script.py** -> example Python script that creates some synthetic data and generates a figure to include in the manuscript.

# Work on the project
## Start a repo
The first step in starting out project is to start a version-controlled directory or a **repository** (_repo_ in short). 

There are two ways to start a repo:

1. Starting it from scratch (locally), and connecting it to a remote repo
2. Cloning an already-existing remote repo to your local computer

We can start a repo from scratch with the command:
```
git init
```

Alternatively, you can clone an existing remote repo by using the command:
```
git clone https://gitlab.com/mv_group/version_control_tutorial.git
```
You will likely use this method if you want to contribute to another colleague's work or if someone else created the repository already and you need to work on it. In this tutorial we are going to assume you want to **work mostly independently** and so you will start from scratch in your local machine.

Note that both methods will create a version-controlled folder with Git in the background controlling **all the contents and versions at all times**.

### First steps
It's very important to always include a **descriptive** _Readme file_ in your repository, to present to you and others what the repository is for and what are its contents. Together with that let's start all the files we will need for our project.

After I created the folders and the files that I will need for my manuscript.
```
$ git init
Initialized empty Git repository in path_to/manuscript_repo/.git/

$ git status
On branch main

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        Readme.md
        manuscript.tex
        python_script.py

nothing added to commit but untracked files present (use "git add" to track)
```

As you can see, Git recognized **we have created a new repo** and that this folder has **new untracked files**.

## The workflow
Everything with Git is divided in **branches**. Git branches, like tree branches, they start from the main "stalk", and grow outwards. Typically, each branch contains a **feature** that is _different or independent enough_ from the main project that can be treated separately. 

By default the main "stalk" or branch in Git is called the **main branch**. You will see this name popup every here and there.

### The first commit
So far we have created the new files but we haven't yet told Git to "save" the "status" or _version_ of these files for future reference. This is called a **commit**. So let's do our first commit to jumpstart our project.

```
$ git add .

$ git commit -m "first commit to start repository"
[main (root-commit) c314709] first commit to start repository
 3 files changed, 1 insertion(+)
 create mode 100644 Readme.md
 create mode 100644 manuscript.tex
 create mode 100644 python_script.py

$ git status
On branch main
nothing to commit, working tree clean
```

The first command will add all your new files to the **staging area**, a place where the files are ready to be commited. The second command will *commit* the versions of added files to the version control memory associating them with a _message_ in quotation. 

This messages are **mandatory** and are crucial to keep order and to help yourself remember what was done in each commit. It is easier to remember and understand the history of your workflow if you documented well your commits with clever use of the commit messages.

### Checking the history
Whenever you are lost, you don't remember the history of the project, or simply you need to find out when you did what, you can resort to the Git log. It will inform you on all commits executed in this repo, with date, time, author information and commit messages.

```
$ git log
commit c314709dd31feadbc0e091d5f62286a279992fc5 (HEAD -> main)
Author: marcodallavecchia <marcodv93@gmail.com>
Date:   Sat Jul 24 16:47:35 2021 +0200

    first commit to start repository
```

It is very important that through this tutorial you will get familiar with the different Git outputs and what some of the terms mean. A term you will find often is HEAD, a pointer to the **current branch reference**, in this case the current status of your repo. Another important thing to realize is that each commit is defined by a very long hash code, that **uniquely identify a commit from any other**. You can see that long code for my first commit in the section above.

### Connect to a remote repository
So far we have only worked locally. This means that although all our files inside the repo are version-controlled, the project is not online and is not accessible by anyone else that is not at your computer. To do so we will now see how you can push your repository to a Git manager such as Gitlab.

1. On [gitlab.com](https://gitlab.com/gitlab-com), login with your account and create a new project.
2. Following the Blue Clone button or the instructions below find the SSH or HTTPS address for this new repository.
3. Establish the connection between the local and the remote repo by adding the origin (the remote address) to Git
```
$ git remote add origin git@gitlab.com:marcodv93/manuscript_repo.git
```

4. Push your repository status to the newly-added remote repository
```
$ git push -u origin main
```

## Let's write the manuscript
Usually a manuscript for a publication is divided in many sections, but for simplicity in our case we will only have an _abstract_, an _introduction_ and a _results and discussion_ sections.

A clever way to see these sections is to consider them as **separated features**, meaning that when working on one of these sections we won't work on the others as they are _independent_. This is a very good practice because it prevents clashing of content when working together with collaborators or even when you work by yourself on different sections at different times.

### Creating branches
Whenever we can identify independent features that can benefit from separate development we can create a new branch. This branch will continue developing until is completed, in this way the main branch doesn't have to be changed or update, until the parallel features are completed. This will keep the main branch **clean and organized.**

In this case, we will create and move to a new branch and we will work, add, and commit on that branch alone.

```
$ git branch manuscript_abstract

$ git checkout manuscript_abstract
Switched to branch 'manuscript_abstract'

$ git status
On branch manuscript_abstract
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   manuscript.tex

no changes added to commit (use "git add" and/or "git commit -a")

$ git add .

$ git commit -m "added abstract to manuscript"
[manuscript_abstract c73ed52] added abstract to manuscript
 1 file changed, 23 insertions(+)
```

As you can see, we have created an abstract-specific branch, modified the manuscript.tex file with the abstract, then proceed with the usual workflow: adding and commiting. If we also want to push this commit to the remote repository, we should create a remote branch as well by doing:

```
$ git push origin manuscript_abstract
```

To remember well let's try to do a similar thing for the results and discussion section of our manuscript:

```
$ git checkout main
Switched to branch 'main'

$ git branch manuscript_results_discussion

$ git checkout manuscript_results_discussion
Switched to branch 'manuscript_results_discussion'
$ git status

On branch manuscript_results_discussion
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   manuscript.tex

no changes added to commit (use "git add" and/or "git commit -a")

$ git add .

$ git commit -m ""added results and discussion section to manuscript"
[manuscript_results_discussion a8b1da2] added results and discussion section to manuscript
 1 file changed, 23 insertions(+)
```

Once you have several branches at the same time you can always check the Git log for a specific branch by:

```
$ git log manuscript_abstract
commit c73ed52507d092acfe358c92b7d0b3e1b6c6d6af (manuscript_abstract)
Author: marcodallavecchia <marcodv93@gmail.com>
Date:   Sat Jul 24 17:34:59 2021 +0200

    added abstract to manuscript

commit c314709dd31feadbc0e091d5f62286a279992fc5 (main)
Author: marcodallavecchia <marcodv93@gmail.com>
Date:   Sat Jul 24 16:47:35 2021 +0200

    first commit to start repository
```

### Evaluate differences
Let's now say that you have done many commits in possibly different branches and your project is starting to increase in volume. Yes you can always use Git log to check the commit messages to keep track of what you did but sometimes you forget the details and **you want to check exactly what the differences are between two different versions of a file**.

In my case, while I was working on the python_script.py file I modified my plot to make it look prettier but now I forgot what color it was originally. Let's see how I could check my version chronology to verify what the differences are between my current version and a previous one.

For this task, having the hash code for the commits you want to compare is useful, so you can add the _oneline_ option to the Git log for a brief summary.

```
$ git log --oneline
9740466 (HEAD -> main) modified color and marker size
2d429c5 modified python script
c314709 first commit to start repository

$ git diff HEAD 2d429c5
diff --git a/python_script.py b/python_script.py
index 17c3842..1bd034d 100644
--- a/python_script.py
+++ b/python_script.py
@@ -38,7 +38,7 @@ ax[0].set_xlabel("Y")
 ax[0].set_ylabel("counts")
 ax[0].grid()

-ax[1].scatter(df["X"], df["Y"], c = "purple", s = 90, marker = "*")
+ax[1].scatter(df["X"], df["Y"], c = "blue", s = 50, marker = "*")
 ax[1].grid()
 ax[1].set_ylabel("Y")
 ax[1].set_xlabel("X")
```

What I did here is to use the Git diff command to compare my current status of the directory (HEAD by default) to a previous commit (hash code starts with 2d429c5). What did I learn? It turns out I used to have the color of my plot to purple and my marker size at 90 but I later changed the color to blue and the size to 50. You can learn more about the Git diff output [here](https://www.atlassian.com/git/tutorials/saving-changes/git-diff).

As a tip you can also use the following syntax if you don't want to use the absolute hash code of the commit.

```
$ git diff HEAD~1
```

Git diff is even more powerful that what we have seen so far. For example I can even check the difference between two branches like this:

```
$ git diff manuscript_abstract manuscript_results_discussion
diff --git a/manuscript.tex b/manuscript.tex
index 30875d8..54ea9d6 100644
--- a/manuscript.tex
+++ b/manuscript.tex
@@ -15,9 +15,9 @@

 \maketitle

-\begin{abstract}
-This is a beautiful and descriptive abstract
-\end{abstract}
+\section{Results and Discussion}
+THESE ARE SOME BEAUTIFUL RESULTS WITH SOME AMAZING FIGURES.

+AND LOOK AT THAT DISCUSSION! FLAWLESS!

 \end{document}
\ No newline at end of file
```

You can even compare and evaluate the **difference between two any commits** on your repository. In this case I [redirected](https://linuxhandbook.com/redirection-linux/) the output of this comparison into an external text file to make it easier to go through the differences!

```
$ git log --oneline
9740466 (HEAD -> main) modified color and marker size
2d429c5 modified python script
c314709 first commit to start repository

$ git diff 2d429c5 c314709 > diff.txt
```

### Reverting a mistake
As mentioned, each commit is a safe checkpoint that "remembers" what each file contained at that moment in your repo. This means that you can go back and forth between commits in case you realize you made mistakes or if you simply want to "undo" some progress due to some reasons. Going back and forth between commits needs to be done with attention and care because it can otherwise lead to permanent changes in the history or files of your repo, that might deeply affect your project, especially if collaborative.

In order to prevent this, in this tutorial we will only cover the case in which you made a mistake in your last commit and you wish to undo it.

Of course, a possibility is always to open the file and modify it back to how it was before the last commit **manually** and make a new commit. But, as you can imagine this is error-prone, you might not remember what you changed and it's not sustainable for large projects.

So the best way to do so is to use Git revert. In my simple case I realized the change in a formula in my Python script was wrong and it didn't make sense!

```
$ git diff HEAD~1
diff --git a/python_script.py b/python_script.py
index 1bd034d..14d4921 100644
--- a/python_script.py
+++ b/python_script.py
@@ -19,7 +19,7 @@ X = np.random.normal(loc = 2, scale = 0.4, size=n)
 error = np.random.normal(loc = 0, scale = 0.1, size=n)
 intercept = 3
 slope = -1
-Y = intercept + X*slope + error
+Y = intercept + X**slope - error
```

As you can see I elevated X to slope power and I subtracted instead of adding the error. Such a simple mistake could be easily corrected manually but let's see how to do it with Git revert.

```
$ git revert HEAD
[main 4ec3686] Revert "updated formula"
 1 file changed, 1 insertion(+), 1 deletion(-)

$ git log --oneline
4ec3686 (HEAD -> main) Revert "updated formula"
2a7f663 updated formula
9740466 modified color and marker size
2d429c5 modified python script
c314709 first commit to start repository
```

The first line simply "undoes" any modification that was done during the last commit and applies on top of the current version (the HEAD). But in doing so, it creates a new commit so the modification can be easily tracked and nothing is lost.

## Putting things together
Let's now say that you have completed the work in each of your sections (abstract, introduction etc..) and you are ready to integrate these features into your main manuscript, to render the final file you will submit to the journal. Keep in mind that as long as you work in independent branches, the main branch won't have those features, so it's not time to put things together.

Make sure you are on **the branch you want to merge INTO**. In this case the main branch.

```
$ git checkout main
Switched to branch 'main'

$ git merge manuscript_abstract
Merge made by the 'recursive' strategy.
 manuscript.tex | 23 +++++++++++++++++++++++
 1 file changed, 23 insertions(+)
```

This last command merged all the modifications I made in my abstract branch into the main branch, and the file manuscript.tex file will now contain all the features from the main and the abstract branches!

# Conclusions
There is more to learn in the world of Git and Gitlab, but we hope that this was enough to illustrate the usefulness and proper practices of version control.
